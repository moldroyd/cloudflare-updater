package uk.co.michaeloldroyd.config;

import com.mhackner.cloudflare.CloudFlareClient;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import uk.co.michaeloldroyd.service.CloudflareService;
import uk.co.michaeloldroyd.service.DynamicDnsService;

@Setter
@Component
@ConfigurationProperties("cloudflare.api")
public class CloudflareConfig {
    private String username;
    private String authkey;
    private String domain;
    private String hostname;

    @Bean
    public CloudFlareClient getCloudFlareClient() {
        return new CloudFlareClient(this.authkey, this.username);
    }

    @Bean
    public DynamicDnsService getDnsService(@Autowired CloudFlareClient client) {
        CloudflareService service = new CloudflareService(client);
        service.setDomain(domain);
        service.setRecordName(hostname);
        return service;
    }
}
