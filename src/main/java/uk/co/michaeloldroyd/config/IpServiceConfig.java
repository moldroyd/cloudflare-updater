package uk.co.michaeloldroyd.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import uk.co.michaeloldroyd.service.IcanhazipService;
import uk.co.michaeloldroyd.service.IpResolutionService;
import uk.co.michaeloldroyd.service.STUNService;

import javax.annotation.PostConstruct;
import java.net.URISyntaxException;

@Getter
@Configuration
@ConfigurationProperties("ipservice")
public class IpServiceConfig {
    @NestedConfigurationProperty
    private HttpConfig http;

    @PostConstruct
    public void setDefaultHttpConfig() throws URISyntaxException {
        if (http == null) {
            this.http = new HttpConfig("https://icanhazip.com");
        }
    }

    @Bean
    @Qualifier("STUN")
    @ConditionalOnProperty(value = "ipservice.mode", havingValue = "stun")
    public IpResolutionService getIpService() {
        return new STUNService();
    }

    @Primary
    @Bean
    @Qualifier("HTTP")
    @ConditionalOnProperty(value = "ipservice.mode", havingValue = "http", matchIfMissing = true)
    public IpResolutionService getHttpIpService(IpServiceConfig config, SimpleClientHttpRequestFactory httpClient) {
        return new IcanhazipService(config, httpClient);
    }
}
