package uk.co.michaeloldroyd.config;

import lombok.Getter;

import java.net.URI;
import java.net.URISyntaxException;

@Getter
public class HttpConfig {
    private URI host;
    public HttpConfig(String host) throws URISyntaxException {
        this.host = new URI(host);
    }
}
