package uk.co.michaeloldroyd;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
@EnableAutoConfiguration
public class Application {
    private static final String PROPERTIES_FILE = "cloudflare";

    public static void main(String args[]) {
        new SpringApplicationBuilder(Application.class)
                .properties("spring.config.name:" + PROPERTIES_FILE)
                .build()
                .run(args);
    }
}
