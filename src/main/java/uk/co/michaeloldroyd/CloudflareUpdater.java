package uk.co.michaeloldroyd;

import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import uk.co.michaeloldroyd.service.DynamicDnsService;
import uk.co.michaeloldroyd.service.IpResolutionService;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Log4j
@Service
public class CloudflareUpdater {
    private IpResolutionService ipService;

    private DynamicDnsService dnsService;

    public CloudflareUpdater(
        @Autowired IpResolutionService ipService,
        @Autowired DynamicDnsService dnsService
    ) {
        this.ipService = ipService;
        this.dnsService = dnsService;
    }

    @Scheduled(cron="0 0/1 * * * *")
    @ConditionalOnProperty(value = "scheduling.enabled", havingValue = "true", matchIfMissing = true)
    public void checkIpForChange() {
        String currentAddress;
        InetAddress currentExternalAddress;
        try {
            currentAddress = dnsService.checkRecord();
            currentExternalAddress = ipService.getExternalIpAddress();
            log.debug("Current Mapped Address: " + currentAddress);
            log.debug("Current External Address: " + currentExternalAddress.getHostAddress());
            if (!currentAddress.equals(currentExternalAddress.getHostAddress())) {
                log.info("Address mismatch. Updating DNS entry...");
                dnsService.updateRecord(currentExternalAddress.getHostAddress());
                log.info("Changed "+currentAddress+" to "+currentExternalAddress.getHostAddress());
            }
        } catch (UnknownHostException e) {
            log.error("Unknown Host: " + e.getMessage());
        } catch (RuntimeException e) {
            log.error(e.getMessage());
        }
    }
}
