package uk.co.michaeloldroyd.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import uk.co.michaeloldroyd.config.IpServiceConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

@Log4j2
public class IcanhazipService implements IpResolutionService {
    private SimpleClientHttpRequestFactory httpClient;

    private IpServiceConfig config;

    public IcanhazipService(IpServiceConfig config, SimpleClientHttpRequestFactory httpClient) {
        this.config = config;
        this.httpClient = httpClient;
    }

    @Override
    public InetAddress getExternalIpAddress() {
        try {
            ClientHttpRequest request = httpClient.createRequest(config.getHttp().getHost(), HttpMethod.GET);
            ClientHttpResponse response = request.execute();
            String host = getHostFromResponseBody(response);
            return InetAddress.getByName(host);
        } catch (IOException e) {
            throw new RuntimeException("Unable to determine IP: " + e.getMessage(), e);
        }
    }

    private String getHostFromResponseBody(ClientHttpResponse response) throws IOException {
        String host = "localhost";
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getBody()))) {
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                if (!line.isEmpty()) {
                    host = line;
                }
            }
        }
        return host;
    }
}
