package uk.co.michaeloldroyd.service;

import java.net.InetAddress;
import java.net.UnknownHostException;

public interface IpResolutionService {
    InetAddress getExternalIpAddress() throws UnknownHostException;
}
