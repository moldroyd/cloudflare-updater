package uk.co.michaeloldroyd.service;

import java.net.InetAddress;

/**
 * Created by oldroydm on 06/02/2017.
 */
public interface DynamicDnsService {
    String checkRecord();
    Boolean updateRecord(String address);
}
