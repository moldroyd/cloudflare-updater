package uk.co.michaeloldroyd.service;

import lombok.Setter;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class DummyDnsService implements DynamicDnsService {
    private InetAddress publicAddress = null;

    public static final int UPDATE_FAILURE = 1;
    public static final int UPDATE_SUCCESS = 2;

    public DummyDnsService() {
        setPublicIp("127.0.0.1");
    }

    @Override
    public String checkRecord() {
        return publicAddress.getHostAddress();
    }

    @Override
    public Boolean updateRecord(String address) {
        return null;
    }

    public void setPublicIp(String address) {
        try {
            publicAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            throw new RuntimeException("Bad Address: " + address);
        }
    }
}
