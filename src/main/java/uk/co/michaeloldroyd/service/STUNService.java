package uk.co.michaeloldroyd.service;

import lombok.extern.log4j.Log4j;
import net.java.stun4j.StunAddress;
import net.java.stun4j.StunException;
import net.java.stun4j.client.NetworkConfigurationDiscoveryProcess;
import net.java.stun4j.client.StunDiscoveryReport;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Log4j
public class STUNService implements IpResolutionService {

    private NetworkConfigurationDiscoveryProcess client;

    @Override
    public InetAddress getExternalIpAddress() throws UnknownHostException {
        StunAddress localAddress = new StunAddress(InetAddress.getLocalHost(), 64000);
        StunAddress remoteAddress = new StunAddress(InetAddress.getByName("stun.l.google.com"), 3479);

        client = new NetworkConfigurationDiscoveryProcess(localAddress, remoteAddress);

        try {
            client.start();
            StunDiscoveryReport result = client.determineAddress();
            log.debug(result);
            return InetAddress.getByName(result.getPublicAddress().toString());
        } catch (StunException e) {
            throw new UnknownHostException("Failed to determine IP: " + e.getMessage());
        } catch (IOException e) {
            throw new UnknownHostException("Failed to connect: " + e.getMessage());
        } finally {
            client.shutDown();
        }
    }
}
