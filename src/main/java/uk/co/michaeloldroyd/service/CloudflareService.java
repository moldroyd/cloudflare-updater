package uk.co.michaeloldroyd.service;

import com.mhackner.cloudflare.CloudFlareClient;
import lombok.Setter;
import lombok.extern.log4j.Log4j;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j
public class CloudflareService implements DynamicDnsService {
    private CloudFlareClient client;

    @Setter
    private String recordName;

    @Setter
    private String domain;

    private String recordId;
    private String dnsZone;

    public CloudflareService(CloudFlareClient client) {
        this.client = client;
    }

    public String checkRecord() {
        try {
            return getExistingRecord();
        } catch (UnknownHostException e) {
            throw new RuntimeException("Unknown Host: " + recordName, e);
        }
    }

    public Boolean updateRecord(String address) {
        Map<String, String> record = new HashMap<>();
        record.put("type", "A");
        record.put("name", recordName);
        record.put("zone_id", dnsZone);
        record.put("id", recordId);
        record.put("content", address);
        client.updateRecord(record);
        return false;
    }

    private String getExistingRecord() throws UnknownHostException {
        Map zone = client.getZone(domain);
        dnsZone = zone.get("id").toString();
        List<Map> sub = client.getRecords(dnsZone, getParams());
        if (sub.size() == 1) {
            recordId = sub.get(0).get("id").toString();
            return sub.get(0).get("content").toString();
        }
        throw new UnknownHostException();
    }

    private Map<String, String> getParams() {
        Map<String, String> params = new HashMap<>();
        params.put("name", recordName);
        params.put("type", "A");
        return params;
    }
}
