package uk.co.michaeloldroyd.service;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class DummyIpService implements IpResolutionService {
    @Override
    public InetAddress getExternalIpAddress() throws UnknownHostException {
        return InetAddress.getByName("127.0.0.1");
    }
}
