# Cloudflare Updater - Dynamic DNS updater
A daemon enabling periodic checking of external IP address of the host, and update through a configurable Dynamic DNS 
provider. The only supported DNS provider is currently Cloudflare—as the name suggests—however more providers could
be implemented in the future as per requirements and demand.

## Installation (Debian)
The application is currently available as a debian package. You can install it using the following command;
```bash
$ sudo dpkg -i cloudflare-updater_1.0.0-1.deb
```
The daemon runs as a service user `cf-updater`. The daemon is written in Java, and operates best when utilising Oracle's
Java 8 distribution. It does seem to work with OpenJDK, but is much slower and there are problems communicating with
Cloudflare's API using older releases.

After installing the package to your system, you are provided with a skeleton properties file `/etc/cf-updater/cloudflare.properties`. 
The following properties are currently read from this file;

* `cloudflare.api.domain` The TLD registered with Cloudflare
* `cloudflare.api.hostname` The (sub) domain record you wish to synchronize
* `cloudflare.api.endpoint` The Cloudflare API endpoint (currently https://api.cloudflare.com/client/v4/)
* `cloudflare.api.username` Your Cloudflare account username
* `cloudflare.api.authkey` Your Cloudflare account API key
* `cloudflare.api.zonekey` Your TLD zone key
* `http.timeout.connect` Connection timeout (milliseconds)
* `http.timeout.read` Connection read timeout (milliseconds)
* `ipservice.http.endpoint` An endpoint which returns your public IP, such as https://icanhazip.com/

After adding the above configuration, you will need to restart the service;

```bash
$ sudo service cf-updater restart
```

## Logging
The package also inserts a rsyslogd configuration to redirect logs for the daemon to `/var/log/cf-updater.log`.

## Systemd
If you are too enlightened to use systemd (for whatever reason) then you are probably also able to figure out how to
configure it. Good luck with that. Feel free to submit a merge request, I'd be more than happy to review it.

## Stability
I've had the application running for a few months on a Raspbarry Pi 3 model B, and it is quite stable. I see occasional
errors with various HTTP connections failing but this is expected. The error handling is not brilliant, but a work in
progress.

## Testing
Coming soon in version 2, the complete greenfield rewrite.